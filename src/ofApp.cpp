#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    ofBackground(0);
    int rad = 50;
     for(int degree = 0; degree < 360; degree += 3){
         
         balls[degree/3].setPosition(glm::vec3(rad * cos(degree * DEG_TO_RAD),
                                               rad * sin(degree * DEG_TO_RAD), 0));
         ballColors[degree/3].setHsb(degree/3*255/120, 255, 255);
     }
    
   
    cam.setPosition(10, 0, 150);
    ofSetSmoothLighting(true);
     
    light1.setDiffuseColor(ofColor(255, 250, 200));
    light1.setPosition(-100, 100, 500);
    light1.enable();
    
    
    light2.setDiffuseColor(ofColor(155, 150, 230));
    light2.setPosition(-400, -200, 000);
    light2.enable();
    

    
    ofEnableAlphaBlending();
    ofEnableDepthTest();
    
    gui.setup();
    gui.add(noise.setup("Noise", 5, 0, 15));
//
//    light1.setDiffuseColor(ofColor(255, 250, 200));
//    light1.setPosition(-100, 100, 500);
//    light1.enable();
   
       
//    light2.setDiffuseColor(ofColor(155, 150, 230));
//     light2.setDiffuseColor(ofColor(240, 180, 230));
//    light2.setPosition(-400, -200, 000);
//    light2.enable();
}

//--------------------------------------------------------------
void ofApp::update(){
   
    noiseTime+=0.03;
    for (int i = 0; i < 120; i++) {
        glm::vec3 pos = balls[i].getPosition();
        float myNoise = noise*ofSignedNoise(pos.x/4.0 - noiseTime, pos.y/4.0, 0.0, noiseTime);
        if (myNoise < 0) myNoise = 0;
        balls[i].setRadius(myNoise);
       
    }
    

           
}


//--------------------------------------------------------------
void ofApp::draw(){
     gui.draw();
    cam.begin();
    ofEnableDepthTest();
    ofEnableLighting();
    light1.enable();
    //mat.begin();

  
    for(int i = 0; i < 120; i++) {
        ofSetColor(ballColors[i]);
        balls[i].draw();
    }

    //mat.end();
    ofDisableLighting();
    ofDisableDepthTest();

    cam.end();

    cam.draw();

    
}
    

