#pragma once

#include "ofMain.h"
#include "ofxGui.h"



class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

 
    
    ofSpherePrimitive balls[120];
    ofColor ballColors[120];
    ofEasyCam cam;
    ofLight light1, light2;
    float noiseTime;
    ofxPanel gui;
    ofxIntSlider noise;
};
